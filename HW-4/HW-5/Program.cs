﻿// See https://aka.ms/new-console-template for more information
int chetNumber = 0; //количество четных чисел
int nechetNumber = 0; //количество нечетных числе
int SumChetNumber = 0; //Сумма четных чисел
int SumNechetNumber = 0; //Сумма нечетных чисел

int[] array= new int[10];
for (int i = 0;  i < array.Length;  i++)
{
    Console.Write($"Введите элемент массива с индексом {i}:\t ");
    array[i] = int.Parse(Console.ReadLine());
    if (array[i] < 0)
    {
        Console.WriteLine("Необходимо вводить положительные числа элемента массива");
        i--;
    }

    else if (array[i] % 2 == 0)
    {
        chetNumber++;
        SumChetNumber = SumChetNumber + array[i];
    }
    else
    {
        nechetNumber++;
        SumNechetNumber = SumNechetNumber + array[i];
    }
}

Console.WriteLine("Вывод элементов массива:");
for (int i = 0; i < array.Length; i++)
{
    Console.WriteLine(array[i]);
}


Console.WriteLine("Количество четных чисел:" + chetNumber);
Console.WriteLine("Количество нечетных чисел:" + nechetNumber);
Console.WriteLine("Сумма четных чисел:" + SumChetNumber);
Console.WriteLine("Сумма нечетных чисел:" + SumNechetNumber);

Console.ReadLine();