﻿// See https://aka.ms/new-console-template for more information


int[] array= new int[5]{1,4,5,8,9};

Console.WriteLine("Элементы заданного массива:");
for (int i = 0; i < array.Length; i++)
{
    Console.WriteLine(array[i]);
}


var sum=Sum(array);
Console.WriteLine("Сумма элементов массива: " +sum);
//метод подсчета суммы элементов массива
int Sum(params int[] array)
{
    int s = 0;
    for (int i = 0; i < array.Length; i++)
    {
        s = s + array[i];
    }

    return s;
}


Console.WriteLine("Введите параметр для вывода массива, где 1 -вертикальный вывод, 0-горизонтальный:");
string a = Console.ReadLine();
int vyvod = Convert.ToInt32(a);

Print(vyvod);

void Print(int vyvod) //метод вывода массива вертикально или горизонтально
{
    if (vyvod == 1)
    {
        Console.WriteLine("Вывод элементов массива вертикально:");
        for (int i = 0; i < array.Length; i++)
        {
            Console.WriteLine(array[i]);
        }
    }
    else if (vyvod==0)
    {
        Console.WriteLine("Вывод элементов массива горизонтально:");
        for (int i = 0; i < array.Length; i++)
        {
            Console.Write(array[i]+" ");
        }
    }
}


Console.ReadLine();